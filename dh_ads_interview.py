#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np


# In[ ]:


PARENT_PATH = "C:/Users/T8MRH/Downloads/"
CASUAL_LOOKUP = "dh_causal_lookup.csv"
TRANSACTIONS = "dh_transactions.csv"
STORE_LOOKUP = "dh_store_lookup.csv"
PRODUCT_LOOKUP = "dh_product_lookup.csv"


# In[ ]:


casual_df = pd.read_csv(PARENT_PATH+CASUAL_LOOKUP)
casual_df.head()


# In[ ]:


trans_df = pd.read_csv(PARENT_PATH+TRANSACTIONS)
trans_df.head()


# In[ ]:


store_df = pd.read_csv(PARENT_PATH+STORE_LOOKUP)
store_df.head()


# In[ ]:


product_df = pd.read_csv(PARENT_PATH+PRODUCT_LOOKUP)
product_df.head()


# In[ ]:


#1. TOP 5 PRODUCTS IN EACH COMMODITY (BY dollar_sales)

# Gather product and sales information
product_names = product_df[['upc','product_description','commodity']].drop_duplicates()
product_sales = trans_df[['upc','dollar_sales']]
# Combine product and sales information
product_base = product_names.merge(product_sales, how = 'outer')
# Aggregate the sales data 
product_agg = product_base.groupby(['upc','product_description','commodity']).agg({'dollar_sales':sum}).reset_index()
# Output top 5 products (based on dollar_sales) per commodity
product_agg.sort_values('dollar_sales',ascending = False).groupby(['commodity']).head(5).sort_values(['commodity','dollar_sales'], ascending = False).set_index(pd.Index(list(range(1,6))*4))


# In[ ]:


#2. TOP 5 BRANDS IN EACH COMMODITY (BY dollar_sales)

# Gather product and sales information
product_names = product_df[['upc','brand','commodity']].drop_duplicates()
product_sales = trans_df[['upc','dollar_sales']]
# Combine product and sales information
product_base = product_names.merge(product_sales, how = 'outer')
# Aggregate the sales data 
product_agg = product_base.groupby(['brand','commodity']).agg({'dollar_sales':sum}).reset_index()
# Output top 5 products (based on dollar_sales) per commodity
product_agg.sort_values('dollar_sales',ascending = False).groupby(['commodity']).head(5).sort_values(['commodity','dollar_sales'], ascending = False).set_index(pd.Index(list(range(1,6))*4))


# In[ ]:


#3. How often is each commodity purchased on average by a customer?

# Gather product and sales information
product_names = product_df[['upc','commodity']].drop_duplicates()
product_sales = trans_df[['upc','household','units','week']]
# Combine product and sales information
product_base = product_names.merge(product_sales, how = 'outer')
# Aggregate the sales data 
product_agg = product_base.groupby(['household','commodity']).agg({'units':'sum','week':'count'}).reset_index().rename(columns={'week':'num_weeks'})
# Average units purchased per week
product_agg['units_per_week'] = product_agg['units'] / product_agg['num_weeks']
# Average units purchased per week per customer for each commodity
product_agg.groupby(['commodity']).agg({'units_per_week':np.mean}).reset_index().rename(columns={'units_per_week':'units_per_week_per_household'})


# In[ ]:


#4. How is the performance of the Pasta category? (Hint: by geography, weekly trends, etc)

# Gather product and sales information
pasta_products = list(product_df[product_df['commodity'] == 'pasta']['upc'].drop_duplicates())
pasta_trans = trans_df[trans_df['upc'].isin(pasta_products)]
# Total Pasta Sales by Geography
pasta_trans.groupby('geography').agg({'dollar_sales':'sum'}).reset_index()
# It appears that pasta sales are better in region 1 than in region 2


# In[ ]:


# Total Pasta Sales per Week
pasta_trans.groupby('week').agg({'dollar_sales':'sum'}).reset_index().plot(x ='week', y='dollar_sales', kind = 'line')
# It appears that pasta sells the best during the middle weeks of the year (summer time in the northern hemisphere)


# In[ ]:


# Total Pasta Sales per week per geography
pasta_trans.groupby(['week','geography']).agg({'dollar_sales':'sum'}).reset_index().pivot(index='week', columns='geography', values='dollar_sales').plot()
# It appears that pasta sales trends in both regions are relatively the same with geography 1 consistently outperforming geography 2


# In[ ]:


#5. In Pasta and Pasta Sauce, what products, if any, are commonly purchased together in same basket?

product_names = product_df[['upc','product_description','commodity']].drop_duplicates()

pasta_and_sauce_trans = trans_df.merge(product_names, how = 'outer')
pasta_trans = pasta_and_sauce_trans[pasta_and_sauce_trans['commodity']=='pasta']
pasta_sauce_trans = pasta_and_sauce_trans[pasta_and_sauce_trans['commodity']=='pasta sauce']
pasta_and_sauce_combo_trans = pasta_trans.merge(pasta_sauce_trans, on = 'basket', how = 'inner', suffixes = ['_pasta','_sauce'])
pasta_and_pasta_combo_trans = pasta_trans.merge(pasta_trans, on = 'basket', how = 'inner', suffixes = ['_pasta1','_pasta2'])
sauce_and_sauce_combo_trans = pasta_sauce_trans.merge(pasta_sauce_trans, on = 'basket', how = 'inner', suffixes = ['_sauce1','_sauce2'])


# In[ ]:


# Number of times a specific pasta has been purchased with a specific sauce
pd.DataFrame(pasta_and_sauce_combo_trans[['product_description_pasta','product_description_sauce']].value_counts()).reset_index().head()


# In[ ]:


# Number of times one sauce has been purchased with a different sauce
pd.DataFrame(sauce_and_sauce_combo_trans[sauce_and_sauce_combo_trans['product_description_sauce1'] != sauce_and_sauce_combo_trans['product_description_sauce2']][['product_description_sauce1','product_description_sauce2']].value_counts()).reset_index().head()


# In[ ]:


# Number of times one pasta has been purchased with a different pasta
pd.DataFrame(pasta_and_pasta_combo_trans[pasta_and_pasta_combo_trans['product_description_pasta1'] != pasta_and_pasta_combo_trans['product_description_pasta2']][['product_description_pasta1','product_description_pasta2']].value_counts()).reset_index().head()

